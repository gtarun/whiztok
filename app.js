
var express = require('express')
,engine = require('ejs-locals')
,routes = require('./routes/routes')
,http = require('http')
,path = require('path')
,passport = require("passport")
,config = require("./config/appconfig")
,flash = require("connect-flash")
,mongoose = require('mongoose')
,cache = require('./cache/redis_cache')
,static = require('node-static')
,file = new(static.Server)()
,util = require('util')
,socketio=require('socket.io'),
PeerServer = require('peer').PeerServer;

// Initialize the Redis Cache
cache.initialize();
// Initialize the Passport configurations
require('./config/passport')(passport, config);
// Establish a connection to MongoDB.
mongoose.connect(config.development.db);
// Create the Express app
var app = express();
// all environments
// TODO : Current app is based on HTTP. Need to be ported to HTTPS based app.
app.set('port', process.env.PORT || 80);
//app.set('ip',process.env.IP||'172.26.16.246');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('ejs', engine);
app.use(express.favicon("public/images/favicon.ico")); 
app.set('env', 'development');
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.session({
	secret : 'SECRET'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.methodOverride());
app.use(flash());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(express.urlencoded());
// development only
if ('development' === app.get('env')) {
	app.use(express.errorHandler());
}
// Default Error handlers to be used if express is not able to find the correct route
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('500', {
		error : err
	});
});
app.use(function(req, res, next) {
	res.status(404);
	if (req.accepts('html')) {
		res.render('404', {
			url : req.url
		});
		return;
	}
	if (req.accepts('json')) {
		res.send({
			error : 'Not found'
		});
		return;
	}
	res.type('txt').send('Not found');
});
// Load all the routes to be used for express.
require('./routes/routes')(app, passport, cache);
// Start the HTTP server, and listen on configured port.
// TODO : Need to make this as as HTTPS server

var server=app.listen(app.get('port'), function() {

	console.log('Whiztok is listening on port ' + app.get('port'));
});
var io=socketio.listen(server);
io.set('log level', 1);
var clients={};
var socketsOfClients = {};
io.sockets.on('connection',function(socket){
	socket.on('online',function(data){
		userid=data.userid;
		if(clients[userid]===undefined){
			clients[userid] = socket.id;
      		socketsOfClients[socket.id] = userid;
			userJoined(userid);
    	}
		console.log("connected clients are"+JSON.stringify(clients));
	});
	socket.on('disconnect',function(){
		var userid=socketsOfClients[socket.id];
		delete socketsOfClients[socket.id];
		delete clients[userid];
		console.log("in disconected"+JSON.stringify(clients));
		userJoined(userid);
	});
	////////////////polled user to show online////////////////
	socket.on('polled users',function(data){
		var data_send={};
		data_send.offline=[];
		data_send.online=[];
		for (var i=0;i<data.polled.length;i++)
		{
			var id=data.polled[i].id;
			if(clients[id]===undefined){
				data_send.offline.push(id);
			}
			else
			{
				data_send.online.push(id);			
			}
			
		}
		socket.emit('showusers',data_send);
	})

	socket.on('callrequest',function(data){
		//console.log("received on server callrequest");
		
		var srcUser;
        srcUser = socketsOfClients[socket.id];
        console.log("going to send call request to"+clients[data.target]);
		io.sockets.sockets[clients[data.target]].emit('callrequest',
                {"source": srcUser});
    })

	socket.on('callaccepted',function(data){
		//console.log("call accepts"+JSON.stringify(data));
		var srcUser;
		var peerid=data.peerid;
        srcUser = socketsOfClients[socket.id];
		io.sockets.sockets[clients[data.source]].emit('callaccepted',{
			'source':srcUser,
			'callto':peerid,
			'name':data.name,
		});
	})
	socket.on('callrejected',function(data){
		console.log("call rejected"+JSON.stringify(data));
		var srcUser;
        srcUser = socketsOfClients[socket.id];
		io.sockets.sockets[clients[data.target]].emit('callrejected',{
			'source':srcUser,
			'name':data.name
	});
	})
	socket.on('ready',function(data){
		console.log("ready to answer "+JSON.stringify(data));
		var srcUser;
        srcUser = socketsOfClients[socket.id];
		io.sockets.sockets[clients[data.target]].emit('ready',{
			'source':srcUser,
			'peer':data.peer
	});
	})

	socket.on('message', function(msg) {
		console.log('on server message'+JSON.stringify(msg));
		console.log("message received for "+socketsOfClients[clients[msg.target]]);
          var srcUser;
          srcUser = socketsOfClients[socket.id];
          io.sockets.sockets[clients[msg.target]].emit('message',
                {"source": srcUser,
                 "message": msg.message,
                 "sendername":msg.sendername
          		});
          
   })
})


function userJoined(userid) {
	console.log("adding/removing a new user");
    io.sockets.emit('userupdate', { "userid": userid });
}

