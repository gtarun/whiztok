$(document).ready(function(){
	$('.navbar-header').click(function(){
		$("#main_menu").toggle(300,'swing');
	});

	if($('#email').val())
        $('#email').prop('readonly',true);      
	
	$('#nxt').click(function(){
		$('#createUser1').validate({
            focusInvalid: false, 
            ignore: "",
            rules: {
                name:{
                	required : true
                },
                pwd:{
					required: true,
					minlength: 6
				},
				cpwd:{
					required: true,
					equalTo: "#pwd"
				}
            },
		});
		if($('#createUser').valid()){	
			$('.content2').show();
			$('.content1').hide();
		}
	});
});