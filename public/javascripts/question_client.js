$(document).ready(function(){
	var peercall,peerrec,call,peer;
	var socket,loc,destinationid;
	console.log(window.location.search.substring(4));
	var qid=window.location.search.substring(4)
	ques_id=window.location.search.substring(4);
	loc=(window.location.protocol+'//'+window.location.host);
	socket=io.connect(loc);
	//connect a new user
	var id=$('#user_id').val();
	socket.emit('online',{userid:id});
	
	socket.on('userupdate',function(data){
		$.ajax({
			url:'/getPolledUsers',
			data:{'id':ques_id},
			type:'POST',
			dataType: 'json',
			beforesend:function(){},
			error:function(err,a,b){
		    	// console.log("err occured");
		    },
		    success:function(data){
		    	var response=[];
		    	if(data.length>0){
		    		for(var i in data){
		    			response.push(data[i]);
		    		}
		    		socket.emit("polled users",{polled:response});
		    	}
		    }
		})
	})

	socket.on("showusers",function(data){
		$('#online2').html(" ");
		$('#online1').html(" "); 
		$('#offline2').html(" ");
		$('#offline1').html(" ");
		//console.log("total polled users "+JSON.stringify(data));
		var online=data.online;
		var offline=data.offline;
		for(var i=0;i<online.length;i++){
			id=online[i];
			$.ajax({
				url:'/getUserById',
				data:{'id':id},
				type:'POST',
				dataType: 'json',
				beforesend:function(){
					   	// console.log("before sending");
					   },
					   error:function(err,a,b){
					   	// console.log("err occured");
					   },
					   success:function(data){
					  	// console.log("success"+JSON.stringify(data));
					  	fullName=data[0].fullName,
					  	rating=data[0].rating;
					  	id=data[0]._id;
					  	var user='<li>'+
					  	'<span class="online_user">'+
					  	'<input style="display:none" type="text" id="userid" value='+fullName+' />'+
					  	'<input style="display:none" type="text" id="useridchat" value='+id+' />'+
					  	'</span>'+
					  	fullName+
					  	'</li>';
					  	var online='<li>'+
					  	'<span class="rating" data-toggle="tooltip" data-placement="right" title=\"'+rating+'\" ></span>'+
					  	'</li>'
					  	$('#online2').append(user);
					  	$('#online1').append(online);  
					  }
					})
		}

		for(var i=0;i<offline.length;i++){
			id=offline[i];
			$.ajax({
				url:'/getUserById',
				data:{'id':id},
				type:'POST',
				dataType: 'json',
				beforesend:function(){
					   	// console.log("before sending");
					   },
					   error:function(err,a,b){
					   	// console.log("err occured");
					   },
					   success:function(data){
					  	// console.log("success"+JSON.stringify(data));
					  	fullName=data[0].fullName,
					  	rating=data[0].rating;
					  	var user='<li>'+
					  	'<span class="online_user1"></span>'+
					  	fullName+
					  	'</li>';
					  	var online='<li>'+
					  	'<span class="rating" data-toggle="tooltip" data-placement="right" title=\"'+rating+'\" ></span>'+
					  	'</li>'
					  	$('#offline2').append(user);
					  	$('#offline1').append(online);
					  }
					})		
		}					
	});

$("ul").delegate(".online_user","click", function(){
	var callto=$(this).children();
              //id of online user who you clicked
    destinationid=callto[1].value;
    
    $("#myModal").modal('show');
    $('.receiver').val(destinationid);
    socket.emit('callrequest',{
              	'target':destinationid
    });
});
if (window.existingCall) {
        window.existingCall.close();
      }
socket.on('callrequest',function(data){
			var callresponse;
			var user_name=$('#user_name').val();
			//console.log('callrequest on client'+data);
			$.ajax({
				url:'/getUserById',
				data:{'id':data.source},
				type:'POST',
				dataType: 'json',
				beforesend:function(){
					   	// console.log("before sending");
				},
			    error:function(err,a,b){
					   	// console.log("err occured");
				},
			    success:function(data){
					   	fullName=data[0].fullName,
					   	id=data[0]._id;
					   	callresponse=confirm(fullName+" is calling you for question you polled");
					   	if(callresponse==true){
					  	 		//$('#myModal').modal("show");
				 	 		alert("You have accepted the call.");
				  	 		var peerid=$('#peerid').val();
						  	socket.emit("callaccepted",{
						  		 	 	'source':id,
						  		 	 	'peerid':peerid,
						  		  	 	'name':user_name
						  	})
						}
						else{
						  	alert("You have rejected the call");
						  	socket.emit("callrejected",{
						  		 			'target':id,
						  		 			'name':user_name
						  	})
						}
				}
			})
});
$('#qidmodal').val(ques_id);
socket.on('callrejected',function(data){
		alert("call rejectected by "+data.name);
})
socket.on('callaccepted',function(data){
	alert("callaccepted by " +data.name);
	var callto=data.callto;			
	var peerid=$('#peerid').val();
	var peer = new Peer(peerid, {key: 'lwjd5qra8257b9'});
	peer.on('on',function(){
		console.log("peer created at caller side :-"+peer.id);
	});
	//console.log("peer created at caller side :-"+peer.id);
	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
          // Get audio/video stream
    navigator.getUserMedia({audio: true, video: true}, function(stream){
        // Set your video displays
        $('#localvideo').prop('src', URL.createObjectURL(stream));
        window.localStream = stream;
        var call=peer.call(callto,stream);
        call.on('stream', function(remoteStream) {
		      // Show stream in some video/canvas element
		 	$('#remotevideo').prop('src', URL.createObjectURL(remoteStream));
		  });
        socket.emit("ready",
        {
        	'target':data.source,
        	'peer':data.peer
        });
   	}, function(err)
	   	{ 
	   		console.log("falied to make a call"+err); 
	   	});
});


socket.on('ready',function(data){
	var peerid=$('#peerid').val();
	 // console.log("peer here "+peerid)
	var peery = new Peer(peerid,{ key: 'lwjd5qra8257b9'});
		 // var peerrec = new Peer(peerid,{ key: 'lwjd5qra8257b9'});
	peery.on('on',function(){
		console.log("peer created at receiver side :-"+peer.id);
	});
	$('#myModalrec').modal("show");
	$('.receiver').val(data.source);
	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	peery.on('call', function(call) {
		navigator.getUserMedia({audio: true, video: true}, function(stream){
      		$('#localvideo').prop('src', URL.createObjectURL(stream));
            window.localStream = stream;
            call.answer(stream); // Answer the call with an A/V stream.
	   		call.on('stream', function(remoteStream) {
	   		$('#remotevideo').prop('src', URL.createObjectURL(remoteStream));
	   		});
       });
	});
	});

	$('#close').click(function(){
		$('#chatshow').val(" ");
		window.existingCall.close();
              
    });

    $('#upperClose').click(function(){
		$('#chatshow').val(" ");
		window.existingCall.close();
              
    });
	socket.on('message', function(msg) {
	    //console.log("Message received on receiver is"+JSON.stringify(msg));
	    var innerhtml=$("#chatshow").val()+msg.sendername+' : '+msg.message+'\n';
	    $("#chatshow").val(innerhtml);
	});

	var btn = document.getElementById('chattext');
	btn.onkeydown = function (e) {
	    e = e || window.event;
	    var keyCode = e.keyCode || e.which;
	    if(keyCode==13) {
	       	var user_name=$('#user_name').val();
			var destination=$('.receiver').val();
			var msg=$("#chattext").val();
			if(msg.length>0){
				
				var innerhtml=$("#chatshow").val()+' Me : '+msg+'\n';

			    $("#chatshow").val(innerhtml);
				socket.emit('message',
					{'message':msg ,
					'target':destination,
					'sendername':user_name});
				$("input#chattext").val("");
			}

	    }
	}

	$("#myModal").delegate("#send","click",function(){
		var user_name=$('#user_name').val();
		var destination=$('.receiver').val();
		var msg=$("#chattext").val();
		if(msg.length>0){
			var innerhtml=$("#chatshow").val()+'Me : '+msg+'\n';
		    $("#chatshow").val(innerhtml);
			socket.emit('message',
				{'message':msg ,
				'target':destination,
				'sendername':user_name});
			$("input#chattext").val("");
			$("input#chattext").focus();
		}
	})
})