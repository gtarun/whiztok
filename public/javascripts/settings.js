
$(document).ready(function(){

	$(document).keypress(function(e) {
    	if(e.which == 13) {
        	e.preventDefault();
    	}
	});
	//name changes code 
	$('#editname').on('click',function()
	{	
		if($(this).data('status')=="normal"){
			$('#nametxtbox').val($('#name').html());
			$('#name').hide();
			$('#nametxtbox').show();
			$(this).data('status','edit');
		}
		else{
			$(this).data('status','normal');
			$('#name').html($('#nametxtbox').val());
			$('#name').show();
			$('#nametxtbox').hide();
		}
	});
	
	$('#settings').validate({
        focusInvalid: false, 
                rules: { 
                    nametxtbox: {
                        required: true,                      
                    },
                    pwd:{
                        
                    	minlength:6,
                    },
                    cpwd:{
                        equalTo:'#txtpwd'
                        
                    },
                },
                highlight: function (element) { // hightlight error inputs
                          $(element).css({"border-color": "#E2413E", 
                               "border-width":"1px", 
                                "border-style":"solid"
                                   });
                        },
                        unhighlight: function (element) { // revert the change done by hightlight
                          $(element).css({"border-color": "#4EBA6F", 
                                     "border-width":"1px", 
                                      "border-style":"solid"
                                    });
                        },

                submitHandler: function(form){
                	$.ajax({
                		url:$(form).attr('action'),
                		data:$(form).serialize(),
                		type:'POST',
                		dataType: 'json',
                		cache:false,
                		beforeSend: function(){
                			console.log("before submitting the form");
                		},
                		error:function(jqXHR, textStatus, errorThrown){
                			 var innerHTML = "<div class='alert alert-danger'><button class='close' data-dismiss='alert'></button> Account updation failed. Please try again. </div>" ; 
                                  $("#alerthere").html(innerHTML);
                                  console.log("signup form submission error");
                                  console.log(errorThrown);
                                  console.log(jqXHR);
                                  console.log(textStatus);
                		},
                		success:function(data){
                			console.log(JSON.stringify(data));
                            
                			//var d='<script> setTimeout(function(){window.location.href="/dashboard"});</script>';
                			var d='<div class="col-md-14 alert alert-sucess"><button class="close" data-dismiss="alert"> Account updated </button><script> setTimeout(function(){window.location.href="/dashboard"},3000);</script></div>' ;
                			$("#alerthere").html(d);
                		}
                	})
                }

                })
$('#picturechange').on('click',function(){
    console.log("clicked");
    
})
		
})