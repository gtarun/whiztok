$(document).ready(function(){
  $('#registerform').validate({
               
                ignore: "",
                focusInvalid: false,
                rules: {
                    registerEmail: {
                        minlength: 7,
                        required: true,
                        email: true
                    },
                },
                

                highlight: function (element) { // hightlight error input
                  $(element).css({"border-color": "#E2413E", 
                               "border-width":"1px", 
                                "border-style":"solid"});
                },

                unhighlight: function (element) { // revert the change done by hightlight
                  $(element).css({"border-color": "#4EBA6F", 
                               "border-width":"1px", 
                                "border-style":"solid"});

                },

                submitHandler: function (form) {
                   $.ajax({
                              url : $(form).attr('action'),
                              data : $(form).serialize(),
                              type : 'POST',
                              dataType: 'json', 
                              cache : false,
                              error : function(jqXHR, textStatus, errorThrown) {

                                  var innerHTML = '<div class="alert alert-danger">'+
                                  '<button class="close" data-dismiss="alert">'+
                                  '</button> Email Submission failed.Please try again.</div>' ; 
                                  $("#loginStatus").html(innerHTML);
                              },

                              success : function(data) {
                              if(data.error)
                              {
                                var innerHTML = '<div class = "col-md-10">'+
                                  '<div class="alert alert-danger">'+
                                  '<script>setTimeout(function(){window.location.href="../"},2000);</script>'+
                                   'This email is already registered'+
                                   '</div></div>' ;               
                                $("#signStatus").html(innerHTML);
                              }
                              if(data.email)
                              {
                                var innerHTML = '<div class = "col-md-12">'+
                                 '<div class="alert alert-success">'+
                                 '<script> setTimeout(function(){window.location.href="../"},2000);</script>'+
                                  'Resgistration link has sent to you.\n Please check you email.'+
                                  '</div></div>' ;               
                                $("#signStatus").html(innerHTML);
                                
                              }  
                              return;                                  
                              },

                          });
                        return false;
                }
              });
});
