   // Compatibility shim
  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
  // PeerJS object
  $(document).ready(function(){


        var id=$('#peerid').val();
         //var peer = new Peer(id,{ key: 'lwjd5qra8257b9'});
       // var peer = new Peer(id, {host: '192.168.2.31', port: 9000, path: '/mechat'});


      var peer = new Peer( id,{host: '172.26.28.85', port: 9000, path: '/mechat'});



        
        peer.on('open', function(){
          console.log("my id is "+peer.id);
        });
        // Receiving a call
        peer.on('call', function(call){
          step1();
          var accept=confirm("Incoming call. Do you want to accept?");
          if(accept==true){
            call.answer(window.localStream);
            $('#myModal').modal("show");
            step3(call);
          }
          else{
            alert("You have rejected a call");
          }
        });
        //Receiving an error
        peer.on('error', function(err){
          alert(err.message);
          // Return to step 2 if error occurs
          step2();
        });

        //Click handlers setup
        $(function(){
            $("ul").delegate(".online_user","click", function(){
              step1();
              var callto=$(this).children();
              calltoid=callto[0].value;
              console.log("call to"+calltoid);
              //initiatecall
              var call = peer.call(calltoid, window.localStream);
              step3(call);
            });

            $('#close').click(function(){
              window.existingCall.close();
              step2();
            });

            // Retry if getUserMedia fails
            $('#step1-retry').click(function(){
              $('#step1-error').hide();
              step1();
            });
            // Get things started
             step1();
        });

        function step1 () {
          // Get audio/video stream
          navigator.getUserMedia({audio: true, video: true}, function(stream){
            // Set your video displays
            $('#localvideo').prop('src', URL.createObjectURL(stream));
            window.localStream = stream;
            step2();
          }, function(){ $('#step1-error').show(); });
        }

        function step2 () {
          $('#step1, #step3').hide();
          $('#step2').show();
        }

        function step3 (call) {
          // Hang up on an existing call if present
          if (window.existingCall) {
             window.existingCall.close();
          }
          // Wait for stream on the call, then set peer video display
          call.on('stream', function(stream){
            $('#remotevideo').prop('src', URL.createObjectURL(stream));
          });

          // UI stuff
          window.existingCall = call;
          $('#their-id').text(call.peer);

          call.on('close', step2);
          
          $('#step1, #step2').hide();
          $('#step3').show();
        }

    })