var login = require('../controllers/login_controller.js');
var User = require('../models/users.js');
function initializeRoutes(app,passport,cache){
	
	app.post('/login',passport.authenticate('local', {
		successRedirect : "/dashboard",
		failureRedirect : "/",
		failureFlash : true
	}));
	
	app.get('/logout', function(req, res) {
		User.findOneAndUpdate({_id:req.user._id},{status:0},function(err,data){
			if(err){
				console.log("Error occured");
				res.json(500,err);
			}
			if(data){
				console.log(req.user.fullName +"is offline");
				req.logout();
				res.redirect('/');
			}
		})
		

		
	});

	app.post('/forgotpwd',login.forgotpwd);

	app.get('/auth/facebook',
	  passport.authenticate('facebook'),
	  function(req, res){
	    // The request will be redirected to Facebook for authentication, so this
	    // function will not be called.
	  });
	app.get('/auth/facebook/callback', 
	  passport.authenticate('facebook', { failureRedirect: '/' }),
	  function(req, res) {
	    res.redirect('/dashboard');
	  });

	//------------------github login-----------------
	app.get('/auth/github',
	  passport.authenticate('github'),
	  function(req, res){
	  });
	app.get('/auth/github/callback', 
	  passport.authenticate('github', { failureRedirect: '/' }),
	  function(req, res) {
	    res.redirect('/dashboard');
	  });

//-------------------------google login--------------------
	app.get('/auth/google',
	  passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile',
	                                            'https://www.googleapis.com/auth/userinfo.email'] }),
	  function(req, res){
	  });
	app.get('/auth/google/callback', 
	  passport.authenticate('google', { failureRedirect: '/' }),
	  function(req, res) {
	    res.redirect('/dashboard');
	  });
}

module.exports = initializeRoutes;

