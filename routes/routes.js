var roots_router = require('../routes/roots_router.js');
var admin_router = require('../routes/admin_router.js');
var dashboard_router = require('../routes/dashboard_router.js');
var login_router = require('../routes/login_router.js');
var chat_routes=require('../routes/chat_routes');

module.exports = function initializeRoutes(app, passport, cache)
{

	roots_router(app, passport, cache);
	admin_router(app,passport,cache);
	dashboard_router(app,passport,cache);
	login_router(app,passport,cache);
	chat_routes(app,passport,cache);
}