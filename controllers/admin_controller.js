var User = require('../models/users.js');
var fs=require('fs');

function createUser(req,res){
	console.log("Creating new User");
	
	var response={};
	var newUser = new User();

	newUser.fullName = req.body.fullName;
	newUser.email = req.body.email;
	newUser.password = req.body.pwd;
	newUser.skills = [];
	if(req.body.tags)
	{
		if(typeof(req.body.tags)=="string"){
			newUser.skills.push(req.body.tags);
		}
		else{
			for(var i=0;i<req.body.tags.length;i++)
				{
				var tag =req.body.tags[i];
				newUser.skills.push(tag);
			}
		}
	}

	User.findOne({email:req.body.email},function(err,doc){
		if(err){
			response.error="DB Error";
			res.json(500,response);
		}
		if(!doc)
		{
			newUser.save(function(err,new_user){
					if(err){
						console.log("DB Error"+err);
						response.error="DB Error";
						res.json(500,response);
					}
					if(new_user)
					{
						var path="./public/images/one.jpg";
						fs.readFile(path, function (err, data) {
						  	var newPath = "./public/images/"+new_user._id+".jpg";
						  	fs.writeFile(newPath, data, function (err) {
						  		console.log("Done -saving of pho");
						  	});
						});
						response.value=new_user;
						res.json(200,response);
					}
					else
					{
						response.error="Not saved Try again.";
						res.json(500,response);
					}
				});
		}
		if(doc)
		{
			console.log("user already present");
			response="This eamil already present.";
			res.json(500,response);
		}
	})
}

function isLoggedIn(req, res, next) {
	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated())
		return next();
	// if they aren't redirect them to the home page
	res.redirect('/');
}
exports.createUser=createUser;
exports.isLoggedIn=isLoggedIn;
