var email = require('emailjs');
var crypto = require('crypto');
var redis = require('../cache/redis_cache');

var server = email.server.connect({
	user : "whiztok@gmail.com",
	password : "whiztok123",
	host : "smtp.gmail.com",
	ssl : true
});

function sendMail(email,loc,req,res){
	console.log("Inside sendmail"+email);
	var client = redis.getClient();
	var algo = 'aes256';
	var key = 'anpnzn';
	var cipher = crypto.createCipher(algo,key);
	var token = cipher.update(email,'utf8','hex') + cipher.final('hex');
	
	client.get(token, function(err, reply){
		if (err) {
			console.log(err);
			return;
		}
		if(reply==null){
			console.log("reply is :" +reply);
			redis.insertKeyValue(token, email);

			var url = loc+"?token=" + token;
			var emailText = "Please click on the follwoing link to signup";
			emailText = emailText + "\n" + url;
			
			console.log("new url is :"+ url);
			server.send({
				text : emailText,
				from : "whiztok@gmail.com",
				to   : email,
				subject : "SignUp-Whiztok"
			}, function(err, message) {
				if (err) {
					
				console.log("Error in sending mail");
				res.json(500,err);
				} 
				else {
					console.log("message sent to email");
			}
			});

		}
		else{
			console.log("mail already sent to this email address ");
			res.json(500,err);
		}
	});
}

function decryptToken(req,res){
	var token = req.query.token;
	var algo = 'aes256';
	var key = 'anpnzn';
	var decrypted = "";
	if(token){
		var decipher = crypto.createDecipher(algo,key);
		decrypted = decipher.update(token,'hex','utf8') + decipher.final('utf8');
	}
	else{
		console.log("No token exists");
	}
	
	return decrypted;
}

exports.sendMail=sendMail;
exports.decryptToken = decryptToken;


