var User = require('../models/users.js');
var Question = require('../models/questions.js');

var fs=require('fs');
var moment=require('moment');

function updateChanges(req,res){
	console.log('update updateChanges is'+JSON.stringify(req.body));
	var update={};
	if(req.body.email!=null){
		update.email=req.body.email;
	}
	if(req.body.nametxtbox!=null){
		update.fullName=req.body.nametxtbox;
	}

	if(req.body.pwd!=null && req.body.pwd.length>0 ){
		update.password=req.body.pwd;
	}
	update.skills=[];
	if(req.body.tags)
	{
		if(typeof(req.body.tags)=="string"){
			console.log("type is string");
			update.skills.push(req.body.tags);
		}
		else{
			for(var i=0;i<req.body.tags.length;i++){
				var tag =req.body.tags[i];
				update.skills.push(tag);
			}
		}
	}
	var query = {"_id": req.user._id};
	var options = {};
	User.findOneAndUpdate(query, update, options, function(err, data) {
				if (err) {
    				console.log("DB error"+err);
    				res.json(500,err);
  				}
  		res.json(200,data);
	});
}

function postQuestion(req,res){
	console.log("Post Question");//console.log(req.body.demo.length+req.body.demo);
	if(req.body.demo.length>75){
		var questionData=req.body.demo;
		var tags=req.body.tags;
		var start=questionData.indexOf('Title:');
		var end=questionData.indexOf('</div>');
		start+=6;
		title=questionData.substring(start,end)
		var start=questionData.indexOf('<b>Description:</b>')+"<b>Description:</b>".length;
		var end=questionData.lastIndexOf('</div>');
		desc=questionData.substring(start,end)
		var newQuestion=new Question();
		newQuestion.tags = [];
		newQuestion.title=title;
		newQuestion.description=desc;
		var date=new Date();//console.log("date" +date);

		newQuestion.askedBy.id=req.user._id.toString();
		newQuestion.askedBy.name=req.user.fullName;
		newQuestion.postDate=date;
		if(req.body.tags)
		{
			if(typeof(req.body.tags)=="string"){
				console.log("type is string");
				newQuestion.tags.push(req.body.tags);
			}
			else{
				for(var i=0;i<req.body.tags.length;i++){
					var tag =req.body.tags[i];
					newQuestion.tags.push(tag);
				}
			}
		}
		newQuestion.save(function(err,data){
			if(err){
				console.log("DB error");
				res.json(500,err);
			}
			if(data){
				User.findOneAndUpdate({_id:req.user._id},{$push:{questions:data._id.toString()}},function(err,data){
					if(err){
						console.log("DB Error");
						res.json(500,err);
					}
				});
			}
		})
	}
	res.redirect("/dashboard");
}

function getQuestion(req,res)
{ 
	console.log("get Question");
	Question.find({_id : req.query.id},function(err,data){
		if(err){
			console.log("DB error");
			return err;
		}
		else{
			ques=data[0];

			fs.stat("./public/images/"+req.user._id+".jpg", function(err, stat) {

				if (!err) {
					res.render('question.ejs',{def:"Answer",user : req.user, image:"images/"+req.user._id+".jpg",question : ques});
				    }
				else {
				    res.render('question.ejs',{def:"Answer",user : req.user, image:"images/one.jpg",question : ques});
				}

			});
		}
	});
}

function askedQuestion(req,res){
	console.log("in asked question"+req.user._id);
	Question.find({'askedBy.id':req.user._id},function(err,data)
	{
		if(err){
			console.log("error occured");
			res.json(500,err);
		}
		if(data){
			var questionData={};
			questionData.title=[];
			questionData.description=[];
			questionData.id=[];
			questionData.title1=[];
			questionData.description1=[];
			questionData.id1=[];
			for(var i=0;i<data.length;i++){
				questionData.title.push(data[i].title);
				questionData.description.push(data[i].description);
				questionData.id.push(data[i]._id);
			}
				Question.find({'polledUser':{$elemMatch:{id:req.user._id}}},function(err,polled){
					if(err){
						console.log("error occured");
						res.json(500,err);
					}
					if(polled){
						for(var i=0;i<polled.length;i++){
							console.log(polled[i].title);
							questionData.title1.push(polled[i].title);
							questionData.description1.push(polled[i].description);
							questionData.id1.push(polled[i]._id);
						}
							res.json(200,questionData);
						}
					res.json(200,questionData);
				})
		}
	})
}

function queryId(req,res){
	var id = req.query.id;;
	return id;
}

function getQuestionById(req,res){
	console.log("get Question By Id");
	Question.find({_id:req.body.id},function(err,data){
		if(err){
			console.log("DB error");
			res.json(500,err);
		}
		if(!err){
			var response={};
			response.value=data;
			console.log("here");
			var userids=[];
			for(var i=0;i<data[0].polledUser.length;i++){
				userids.push(data[0].polledUser[i].id);
			}
			User.find({ _id:{ $in: userids }},function(err,data1){
						if(err){
							console.log("error");
						}
						if(data1){
							//console.log("polled user data is"+data1);
							response.user=data1;//console.log("response is "+response.user);
							res.json(200,response);
						}
			});
		}
	});
}

function polling(req,res){
	console.log("going to save polling of answers");
	var update={};
	update.id=req.user._id;
	if(req.body.now=="on"){
		var date = new Date();
		console.log(date);
		date=date.toString();
		var current_hour = date.substring();
		update.time=current_hour;
	}
	else{
		if(req.body.timings.length>0)
		{
			update.time=req.body.timings;
		}
	}
	Question.findOneAndUpdate({_id:req.body.id},{$push:{'polledUser':update}},function(err, data) {
					if (err) {
	    				console.log("DB error"+err);
	    				res.json(500,err);
	  				}
	  				else{
	  					var notification={};
	  					notification.person_id=req.user._id;
	  					notification.time=update.time;
	  					notification.checked=false;
	  					notification.ques_id=req.body.id;
	  					User.findOneAndUpdate({_id:data.askedBy.id},{$push:{'notification':notification}},function(err,data){
	  						if(err){
	  							console.log("Error"+err);
	  						}
	  						if(!err){
	  							console.log("updated data after notification"+data);
	  						}
	  					});
	  				}
	});
	var data="/question?id="+req.body.id
	res.redirect(data);
}	

function changePicture(req,res){
	console.log("changePicture");
	fs.readFile(req.files.image.path, function (err, data) {
	  	var newPath = "./public/images/"+req.user._id+".jpg";
	  	fs.writeFile(newPath, data, function (err) {
	  	res.redirect("/setting");
	  	});
	});
}


function notifications(req,res){
	var time=req.body.qtime;
	console.log("notifications"+req.body.qtime);
	var data=[];
	var response=[];
	response.person=[];
	User.find({_id:req.body.p_id},function(err,data){
		if(err){
			console.log("DB Error");
	 		res.json(500,err);
	 	}
		if(data){
			person_data={
				id:String,
		 		name:String,
				rating:Number,
				title:String,
				time:String
			};
			person_data.id=data[0]._id;
			person_data.name=data[0].fullName;
			person_data.rating=data[0].rating;

			Question.find({_id:req.body.q_id},function(err,q_data){
		 		if(err){
						console.log("DB Error");
						res.json(500,err);
		 		}
		 		if(q_data){
						person_data.q_title=q_data[0].title;
						person_data.q_id=q_data[0]._id;
						person_data.time=time;
		 				res.json(200,person_data);
		 		}
			});
	 	}
	});
}

function countNotify(req,res){
	console.log(req.body.id);
	User.find({_id:req.body.id },function(err,data){
	if(data){
		var count=0;
		console.log('data is in countNotify'+data[0].fullName);
		for(var i=0; i<data[0].notification.length;i++)
		{
			if(data[0].notification[i].checked==false)
				count++;
		}
		console.log("total notifications to read"+count);
		res.json(200,{length:count});
	}
	else
		req.json(200,{length:0});
	})
}

function upvote(req,res){

	console.log("upvote to"+JSON.stringify(req.body));
	User.findOneAndUpdate({_id:req.body.receiver},{$inc:{rating:2}},function(err,data){

		if(err){
			console.log("DB error");
			res.json(500,err);
		}
	
		if(data){
			console.log("updated upvote");
		}
		if(!data)
		{
			console.log("user is not present");
		}
	});

}

function downvote(req,res){

	console.log("downvote to"+req.body.receiver);
	User.findOneAndUpdate({_id:req.body.receiver},{$inc:{rating:-1}},function(err,data){

			if(err){
			console.log("DB error");
			res.json(500,err);
		}
		if(data){
			console.log("updated downvote");
		}
		if(!data)
		{
			console.log("user is not present");
		}
	});
	res.redirect("/dashboard");
}

function satisfiedno(req,res){
		console.log("satisfiedno");
		
	}
function satisfiedyes(req,res){
		console.log("satisfiedyes"+req.body.qid);
		Question.findOneAndUpdate({_id:req.body.qid},{$set:{"answered":true}},function(err,data){
		if(err){
			console.log("DB error");
			res.json(500,err);
		}
		if(data){
			console.log("satisfied");
		}
		if(!data)
		{
			console.log("Question is not present");
		}
		});
		res.redirect("/dashboard");
	}
function satisfied(req,res){
	console.log("satisfied");
	Question.findOneAndUpdate({_id:req.body.id},{$set:{}});
}

function getPolledUsers(req,res){
	console.log("getPolledUsers");
	console.log(req.body.id);
	Question.find({_id:req.body.id},function(err,data){
		if(err){
			console.log(err);
		}
		if(data){
			console.log(JSON.stringify(data[0].polledUser));	
			res.json(200,data[0].polledUser);
		}
	})
}

function getUserById(req,res){
	console.log("getUserById");
	console.log(req.body.id);
	User.find({_id:req.body.id},function(err,data){
			if(!err){
			res.json(200,data);
		}
	});
}


function onlineUser(req,res){
	console.log("checking the users online");
	res.json(200,{data:5});
}

function mygroupAsked(req,res){
	console.log("mygroupAsked"+req.user._id);
	Question.find({'askedBy.id':req.user._id},function(err,data){
		if(err){
			console.log("error occured");
			res.json(500,err);
		}
		if(data){
			var questionData={};
			questionData.title=[];
			questionData.description=[];
			questionData.id=[];
			questionData.status=[];
			for(var i=0;i<data.length;i++){
				questionData.title.push(data[i].title);
				questionData.description.push(data[i].description);
				questionData.id.push(data[i]._id);
				questionData.status.push(data[i].answered);
			}
			res.json(200,questionData);
		}
	})
}

function mygroupPolled(req,res){
	console.log("mygroupPolled");
		Question.find({'polledUser':{$elemMatch:{id:req.user._id}}},function(err,polled){
					if(err){
						console.log("error occured");
						res.json(500,err);
					}
					if(polled){
						var questionData={};
							questionData.title=[];
							questionData.description=[];
							questionData.id=[];
							questionData.status=[];
						for(var i=0;i<polled.length;i++){
							questionData.title.push(polled[i].title);
							questionData.description.push(polled[i].description);
							questionData.id.push(polled[i]._id);
							questionData.status.push(polled[i].answered);
						}
						res.json(200,questionData);
					}
					
		})
}

function alreadyPolled(req,res){
	console.log("alreadyPolled"+req.user._id);
	Question.find({'_id':req.body.id,'askedBy.id':req.user._id },function(err,data){
		if(data.length){
			console.log(data[0].askedBy.id);
			res.json(200,{'id':data[0].askedBy.id});
		}
		else
		{
			console.log('Someone else question');
			Question.find({'_id':req.body.id,'polledUser':{$elemMatch:{id:req.user._id}}},function(err,data){
				console.log(JSON.stringify(data));
				console.log(JSON.stringify(data.length));
				res.json(200,{'data':data.length});
			})
		}
	})

	
}

function updateCheck(req,res){

	console.log("UpdateCheck"+req.body.id+req.user._id);
	var query={'_id':req.user._id ,'notification.ques_id':req.body.id};
	var update={};
	User.findOneAndUpdate(query,{$set:{"notification.$.checked":true}},function(err,data){
		if(data){
			console.log("Update");
			console.log(data)

		}
		else
			console.log('err'+err);
	})
	res.json("done");
}

exports.updateChanges=updateChanges;
exports.getQuestion=getQuestion;
exports.postQuestion=postQuestion;
exports.askedQuestion=askedQuestion;
exports.queryId=queryId;
exports.getQuestionById=getQuestionById
exports.polling=polling;
exports.changePicture=changePicture;
exports.notifications=notifications;
exports.countNotify=countNotify;
exports.upvote=upvote;
exports.downvote=downvote;
exports.satisfied=satisfied;
exports.getPolledUsers=getPolledUsers;
exports.getUserById=getUserById;
exports.onlineUser=onlineUser;
exports.mygroupAsked=mygroupAsked;
exports.mygroupPolled=mygroupPolled;
exports.alreadyPolled=alreadyPolled;
exports.updateCheck=updateCheck;

exports.satisfiedno=satisfiedno;
exports.satisfiedyes=satisfiedyes;

