var redis = require('redis');
var client;

exports.initialize = function() {
	client = redis.createClient();
	client.on("error", function(err) {
		console.log("Error in creating the conenction to Redis Server" + err);
		throw err;
	});
};

exports.getClient = function() {
	return client;
};

exports.insertKeyValue = function(key, value) {
	console.log("The InsertkeyValue called with " + key + ": " + value);
	var lookupKey = key;

	client.set(lookupKey, value, function(error, reply) {
		if (error) {
			console.log("Saving to Redis Server failed.");
		} else {
			console.log("Got response with redis : " + reply);
			client.expire(lookupKey, 3600);
		}
	});
};